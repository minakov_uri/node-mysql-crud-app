                        Install required modules.
The following modules are going to be needed to successfully build the app.

express: used to create handle routing and process requests from the client.
express-fileupload: Simple express file upload middleware that wraps around busboy.
body-parser: used to parse incoming request from the client.
mysql: Node JS driver for MySQL.
ejs: templating engine to render html pages for the app.
req-flash: used to send flash messages to the view
nodemon: Installed globally. It is used to watch for changes to files and automatically restart the server.
Type the following command to install the first 7 modules as dependencies.

`npm install express express-fileupload body-parser mysql ejs req-flash --save`

Then type the following command to install the last module globally on your PC.
nodemon is a tool that helps develop node.js based applications by automatically restarting the node application when file changes in the directory are detected.

Nodemon does not require any additional changes to your code or method of development. 
Nodemon is a replacement wrapper for node, to use nodemon replace the word node on the command line when executing your script.

`npm install nodemon -g`